<?php
class mailjet_api_contact extends mailjet_api {
  /**
   * 
   */
  public function __construct() {
    parent::__construct();
  }
  /**
   * Get general informations about a specific contact.
   * @param array $params
   */
  public function contact_infos(array $params) {
    
  }
  /**
   * Permits your to get all your contacts from different options.
   */
  public function contact_list(array $params = array()) {
    
  } 
  /**
   * Get all your contacts opening your messages.
   */
  public function contact_openers(array $params = array()) {
    
  }  
}
