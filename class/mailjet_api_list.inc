<?php
class mailjet_api_list extends mailjet_api {
  /**
   * 
   */
  public function __construct() {
    parent::__construct();
  }
  /**
   * Add a new contact to one of your lists, by Email or Contact ID.
   */
  public function add_contact(array $params) {
    // Check params
    $check = array(
      'contact' => array(
        '#required' => TRUE,
        '#validators' => array(
           array(
             '#type' => 'OR',
             '#validators' => array(
               'mail',
               'integer'
             )
           ),
         ),
      ),
    );
    if ($this->check_params($params, $check)) {
      $params['contact'] = (array) $params['contact'];
      return $this->add_many_contacts($params);
    }
    else {
      
    }
    return FALSE;
  }
  /**
   * Add multiple contacts to one of your lists, providing a list of Emails. 
   */
  public function add_many_contacts() {
    
  }
  /**
   * Show all your contact lists
   */
  public function all(array $params) {
    
  }
  /**
   * Show all your contacts within a list.
   */
  public function contacts(array $params) {
    
  }
  /**
   * Add a new list of contacts.
   */
  public function create(array $params) {
    // Check params
    $check = array(
      'label' => array(
        '#required' => TRUE,
        '#validators' => array(
           'max_length' => 60,
         ),
      ),
    );
    if ($this->check_params($params, $check)) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  /**
   * Delete one of your contact list.
   */
  public function delete(array $params) {
    
  }
  /**
   * Get a unique internal email used by Mailjet to match all the contacts
   * of one list.
   */
  public function email(array $params) {
    
  }
  /**
   * Remove a contact from a specific or all of your list.
   */
  public function remove_contact(array $params) {
    
  }
  /**
   * Remove multiple contacts from a specific or all of your list.
   */
  public function remove_many_contacts(array $params) {
    
  }
  /**
   * Provide advanced statistics concerning one of your list of contacts.
   */
  public function statistics(array $params) {
    
  }
  /**
   * 
   */
  public function unsub_contact(array $params) {
    
  }
  /**
   * Update your list informations.
   */
  public function update(array $params) {
    
  } 
}