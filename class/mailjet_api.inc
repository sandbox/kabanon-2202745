<?php
class mailjet_api extends Mailjet{
  public $error = array();
  /**
   * 
   */
  public function __construct() {
    if (module_exists('mailjet')) {
      $apiKey = variable_get('mailjet_username');
      $secretKey = variable_get('mailjet_password');
    }
    else {
      $apiKey = variable_get('mailjet_api_username');
      $secretKey = variable_get('mailjet_api_password');
    }
    
    parent::__construct($apiKey, $secretKey);
  }
  /**
   * 
   * @param array $params
   * @param array $check
   */
  protected function check_params(array $params, array $checks) {
    $return = TRUE;
    if (!empty($checks)) {
      foreach($checks as $key => $check) {
        // check required param
        if (isset($check['#required']) && $check['#required'] === TRUE
            && (!isset($params[$key]) || empty($params[$key]))) {
          $this->error[$key] = t('%key is required.', array('%key' => $key));
          $return = FALSE;
        }
        // check validators
        if (isset($check['#validators']) && is_array($check['#validators'])
             && !empty($check['#validators'])) {
          
          $_validators = array();
          
          foreach ($check['#validators'] as $validator_key => $validator_data) {
            // Max length
            if ($validator_key == 'max_length') {
              if (strlen($params[$key]) > $validator_data) {
                $this->error[$key] = t('%key must be lower than %nb caracters.', array('%key' => $key, '%nb' => $validator_data));
                $return = $_validators[] =FALSE;
              }
              else {
                $_validators[] = TRUE;
              }
            }
            // Min length
            if ($validator_key == 'min_length') {
              if (strlen($params[$key]) < $validator_data) {
                $this->error[$key] = t('%key must be greater than %nb caracters.', array('%key' => $key, '%nb' => $validator_data));
                $_validators[] = FALSE;
              }
              else {
                $_validators[] = TRUE;
              }
            }
          }
          // AND / OR ?
          if (isset($check['#type']) && $check['#type'] == 'OR') {
            // OR
            $return = in_array(TRUE, $_validators);
          }
          else {
            // AND
            $return = !in_array(FALSE, $_validators);
          }
          
        }
      }
    }
    if (!$return) {
      foreach ($this->error as $error) {
        drupal_set_message($error, 'error');
      }
    }
    return $return;
  }
}
